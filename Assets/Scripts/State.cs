using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public abstract class State 
{
    protected NavMeshAgent myAgent;
    protected Animator myAnim;
    protected string myInitialAnimationName;
    State currState;
    State prevState;

    public virtual State DoState()
    {
        Debug.Log("doing state "+this.GetType().ToString());  

        return currState;
    }
    public virtual void FinishState()
    {
        Debug.Log("finished state " + this.GetType().ToString());
        myAnim.SetBool(myInitialAnimationName, false);
    }

    public virtual void BeginState()
    {
        Debug.Log("beginning state " + this.GetType().ToString());

        currState = this;
        prevState = currState;

        myAnim.SetBool(myInitialAnimationName,true);
    }

    public virtual void ChangeState(State newState)
    {
        currState.FinishState();

        prevState = currState;
        currState = newState;

        Debug.Log("moving from " + prevState.GetType().ToString() + " to" + newState.GetType().ToString());
        currState.BeginState();
    }
}
