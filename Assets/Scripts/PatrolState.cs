using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : State
{
    public List<Transform> wayPoints;
   
    private Transform currentWayPoint;
    private bool reachedWp = false;

    public PatrolState(Animator anim, NavMeshAgent agent, string InitAnimationName, List<Transform> WPs)
    {
        myAnim = anim;
        myAgent = agent;
        myInitialAnimationName = InitAnimationName;
        wayPoints = WPs;
    }

    public override void BeginState()
    {
        base.BeginState();
        myAgent.isStopped = false;

        #region Initialization and continue to the last saved WP after Idle state 
        if (reachedWp)
        {
            currentWayPoint = RandomizeNextWP();
        }
        else
        {
            if (currentWayPoint == null)
                currentWayPoint = RandomizeNextWP();
        } 
        #endregion
    }

    public override void FinishState()
    {
        base.FinishState();
    }

    public override State DoState()
    {
        myAgent.SetDestination(currentWayPoint.position);
        reachedWp = Mathf.Abs((myAgent.transform.position - currentWayPoint.position).sqrMagnitude) < 2;

        if(reachedWp)
        {
            currentWayPoint = RandomizeNextWP();
            reachedWp=false;
        }
        return base.DoState();
    }

    private Transform RandomizeNextWP()
    {
        int rndIndex;
        Transform rndWP;
        do
        {
            rndIndex = Random.Range(0, wayPoints.Count);
            rndWP = wayPoints[rndIndex];
        } while (rndWP == currentWayPoint || (myAgent.transform.position-rndWP.position).sqrMagnitude < 2);
        return rndWP;
    }
}
