using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class IdleState : State
{
    public IdleState(Animator anim, NavMeshAgent agent, string InitAnimationName)
    {
        myAnim = anim;
        myAgent = agent;
        myInitialAnimationName = InitAnimationName;
    }

    public override void BeginState()
    {
        base.BeginState();
        myAgent.isStopped = true;
    }

    public override void FinishState()
    {
        base.FinishState();
    }

    public override State DoState()
    {
        return  base.DoState();
    }
}
