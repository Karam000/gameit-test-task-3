using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{
    [SerializeField] float IdleStateLifeTime;
    [SerializeField] float PatrolStateLifeTime;

    [SerializeField] NavMeshAgent myAgent;
    [SerializeField] Animator myAnim;
    [SerializeField] string IdleAnimationTransitionName;
    [SerializeField] string PatrolAnimationTransitionName;
    [SerializeField] List<Transform> waypoints;

    IdleState IdleState; 
    PatrolState PatrolState;

    State currentState;

    Coroutine stateSwitcher_Coroutine;
    void Start()
    {
        IdleState = new IdleState(myAnim,myAgent, IdleAnimationTransitionName);
        PatrolState = new PatrolState(myAnim, myAgent, PatrolAnimationTransitionName, waypoints);
        currentState = IdleState;
        IdleState.BeginState();
        stateSwitcher_Coroutine = StartCoroutine(switchState());  //may be used for stop coroutine later on
    }

    void Update()
    {
       currentState = currentState.DoState();
    }

    private IEnumerator switchState()
    {
        while(true)
        {
            currentState.ChangeState(IdleState);
            yield return new WaitForSeconds(IdleStateLifeTime);
            currentState.ChangeState(PatrolState);
            yield return new WaitForSeconds(PatrolStateLifeTime);
        }
    }
}
